/*! \file bannerCompDate.c
 *
 *  \brief Shorten the date for display
 *
 * Sometimes it is desireable to display the date and time of compile
 * on the startup banner.  This function shortens the date returned by
 * the __DATE__ macro to an ISO 8601 date.  This is ony a savings of a
 * single character, but on a 16 character display that can be significant.
 *
 *  \author JJMcD
 *  \date 2013-01-23
 *
 */

#include <stdio.h>
#include <string.h>

/*! Months of the year as returned by __DATE__ */
char mons[12][4] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                     "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

/*! String for returned ISO 8601 date */
char szDate[16];

/*! Shorten the date for display */
/*! Accepts a date in the form mmm dd yyyy.  Copies the year to the
 *  result string and then appends a -. Then looks up the month in
 *  mons[] and adds the index+1, zero fills and again appends a -
 *  to the result.  Finally, appends the day of the month.
 *
 *  \param indate char * - Pointer to the __DATE__ result
 *  \returns Pointer to the result string
 */
char *compDate( char *indate )
{
    char szMonth[3];
    int i;

    memset(szDate,0,sizeof(szDate));
    strcpy(szDate,&indate[6]);
    strcat(szDate,"-");
    strcpy(szMonth,"  ");
    for ( i=0; i<12; i++ )
        if ( !strncmp(mons[i],indate,3) )
            sprintf(szMonth,"%02d",i+1);
    strcat(szDate,szMonth);
    strcat(szDate,"-");
    memcpy(&szDate[9],indate+4,2);
    return szDate;
}
