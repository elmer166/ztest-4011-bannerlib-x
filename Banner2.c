/*! \file
 *
 *  \brief Displays RCR on top, provided string on bottom, both centered
 *
 *  Displays RCR in double-width characters, each double-width character
 *  separated by a single space, centered on the top line.  Displays
 *  the caller-provided string centered on the lower line in
 *  ordinary characters.
 *
 *  \author JJMcD
 *  \date 2013-01-23
 *
 */
#include <string.h>
#include "banner.h"
#include "../include/lcd.h"

/*! Displays RCR on top, provided string on bottom, both centered */
/*! Accepts a string and a display width, displays a double-width
 *  RCR centered on the upper line, and the provided string
 *  in normal characters centered on the bottom line.
 *
 *  Begins by clearing the display, and then loads the first four
 *  locations of CGRAM with the characters to display the large
 *  RCR. Then displays the RCR, separated by spaces, centered on the
 *  top line, and calculates the starting position for the string to
 *  be centered and displays the string on the bottom line.
 *
 *  \param szString char * - String to display on the lower line
 *  \param nDisplayWidth int - Display width in characters
 *  \returns none
 */
void Banner2( char * szString, int nDisplayWidth )
{
    int i;
    int nCursorPosition;

    LCDclear();

    /* Load the CGRAM with the appropriate characters */
    LCDcommand(0x40);
    for ( i=0; i<8; i++ ) LCDletter( banner_rLeft[i] );
    for ( i=0; i<8; i++ ) LCDletter( banner_rRight[i] );
    for ( i=0; i<8; i++ ) LCDletter( banner_cLeft[i] );
    for ( i=0; i<8; i++ ) LCDletter( banner_cRight[i] );

    /* Display the double-width R C R */
    LCDposition((nDisplayWidth-8)/2);
    LCDletter(0);LCDletter(1);
    LCDletter(' ');
    LCDletter(2);LCDletter(3);
    LCDletter(' ');
    LCDletter(0);LCDletter(1);

    /* Calculate the starting position of the caller-provided string*/
    nCursorPosition = (nDisplayWidth - strlen(szString))/2;
    LCDposition(0x40+nCursorPosition);
    /* and display it */
    LCDputs(szString);
}
