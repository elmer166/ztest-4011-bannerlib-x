/* 
 * File:   banner.h
 * Author: jjmcd
 *
 * Created on January 23, 2013, 8:37 AM
 */

#ifndef BANNER_H
#define	BANNER_H
/*! \file banner.h
 *
 *  \brief Definitions for the banner library
 *
 *
 *  \author JJMcD
 *  \date 2013-01-23
 *
 */

#ifdef	__cplusplus
extern "C" {
#endif

/*! Displays RCR on upper left, provided string lower right */
void Banner1( char *, int );
/*! Displays RCR on top, provided string on bottom, both centered */
void Banner2( char *, int );
/*! Display very large RCR with 2 strings to the right */
void Banner3( char *, char *, int );
/*! Display very large RCR with 2 strings to the right - compact version */
void Banner4( char *, char *, int );
/*! Shorten the date for display */
char *compDate( char * );

/* Although these symbols do not need to be exposed to the ultimate
 * application source, they are required here in order to link */
/* Single line, double wide letters */
/*! Left cell of letter R */
extern unsigned char banner_cLeft[8];
/*! Right cell of letter R */
extern unsigned char banner_cRight[8];
/*! Left cell of letter C */
extern unsigned char banner_rLeft[8];
/*! Right cell of letter C */
extern unsigned char banner_rRight[8];
//extern unsigned char wLeft[8];
//extern unsigned char wRight[8];

/* Double line, double wide letters */
/*! Upper left cell of letter R */
extern unsigned char banner_ulR[8];
/*! Upper right cell of letter R */
extern unsigned char banner_urR[8];
/*! Lower left cell of letter R */
extern unsigned char banner_llR[8];
/*! Lower right cell of letter R */
extern unsigned char banner_lrR[8];
/*! Upper left cell of letter C */
extern unsigned char banner_ulC[8];
/*! Upper right cell of letter C */
extern unsigned char banner_urC[8];
/*! Lower left cell of letter C */
extern unsigned char banner_llC[8];
/*! Lower right cell of letter C */
extern unsigned char banner_lrC[8];

#ifdef	__cplusplus
}
#endif

#endif	/* BANNER_H */

