/*! \file Banner3.c
 *
 *  \brief Display very large RCR with 2 strings to the right
 *
 * Displays the letters RCR in double-width, double-high characters on
 * the left of the display.  Two caller provided lines of text are
 * displayed right-justified.  The RCR letters are each separated by
 * a space.
 *
 *  \author JJMcD
 *  \date 2013-01-23
 *
 */
#include <string.h>
#include "../include/lcd.h"
#include "banner.h"

/*! Display very large RCR with 2 strings to the right */
/*! Displays two caller-supplied strings right justified on the
 *  display, the first on the top line and the second on the
 *  second line.  Then loads the CGRAM locations with graphics to
 *  create and R and a C, each taking four positions.  Displays the
 *  double-width double-height characters R C R each separated by
 *  a space.
 *
 *  \param szTopLine char * - String to display on the upper line
 *  \param szBottomLine char * - String to display on the lower line
 *  \param nDisplayWidth int - Display width in characters
 *  \returns none
 */
void Banner3( char *szTopLine, char *szBottomLine, int nDisplayWidth )
{
    int i;

    LCDclear();

    /* Display the first user-supplied string */
    LCDposition( nDisplayWidth-strlen(szTopLine) );
    LCDputs(szTopLine);
    /* Now the second string */
    LCDposition( 0x40+nDisplayWidth-strlen(szBottomLine) );
    LCDputs(szBottomLine);

    /* Load the CGRAM with the data for the large R and C */
   LCDcommand(0x40);
    for ( i=0; i<8; i++ ) LCDletter( banner_ulR[i] );
    for ( i=0; i<8; i++ ) LCDletter( banner_urR[i] );
    for ( i=0; i<8; i++ ) LCDletter( banner_llR[i] );
    for ( i=0; i<8; i++ ) LCDletter( banner_lrR[i] );
    for ( i=0; i<8; i++ ) LCDletter( banner_ulC[i] );
    for ( i=0; i<8; i++ ) LCDletter( banner_urC[i] );
    for ( i=0; i<8; i++ ) LCDletter( banner_llC[i] );
    for ( i=0; i<8; i++ ) LCDletter( banner_lrC[i] );

    /* Display the RCR, each separated by a space */
    LCDhome();
    LCDletter(0);LCDletter(1);LCDletter(' ');
    LCDletter(4);LCDletter(5);LCDletter(' ');
    LCDletter(0);LCDletter(1);
    LCDline2();
    LCDletter(2);LCDletter(3);LCDletter(' ');
    LCDletter(6);LCDletter(7);LCDletter(' ');
    LCDletter(2);LCDletter(3);
}
