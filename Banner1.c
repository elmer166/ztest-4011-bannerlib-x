/*! \file Banner1.c
 *
 *  \brief Displays RCR on upper left, provided string lower right
 *
 *  Displays RCR in double-width characters, each double-width character
 *  separated by a single space, on the left of the top line.  Displays
 *  the caller-provided string right justified on the lower line in
 *  ordinary characters.
 *
 *  \author JJMcD
 *  \date 2013-01-23
 *
 */
#include <string.h>
#include "banner.h"
#include "../include/lcd.h"

/*! Displays RCR on upper left, provided string lower right */
/*! Accepts a string and a display width, displays a double-width
 *  RCR left justified on the upper line, and the provided string
 *  in normal characters right justified on the bottom line.
 *
 *  Begins by clearing the display, and then loads the first four
 *  locations of CGRAM with the characters to display the large
 *  RCR. Then displays the RCR, separated by spaces, and calculates
 *  the starting position for the string to be right justified and
 *  displays the string on the bottom line.
 *
 *  \param szString char * - String to display on the lower line
 *  \param nDisplayWidth int - Display width in characters
 *  \returns none
 */
void Banner1( char * szString, int nDisplayWidth )
{
    int i;
    int nCursorPosition;

    LCDclear();

    /* Load the CGRAM with the appropriate characters */
    LCDcommand(0x40);
    for ( i=0; i<8; i++ ) LCDletter( banner_rLeft[i] );
    for ( i=0; i<8; i++ ) LCDletter( banner_rRight[i] );
    for ( i=0; i<8; i++ ) LCDletter( banner_cLeft[i] );
    for ( i=0; i<8; i++ ) LCDletter( banner_cRight[i] );

    /* Display the double-width R C R */
    LCDposition(0x00);
    LCDletter(0);LCDletter(1);
    LCDletter(' ');
    LCDletter(2);LCDletter(3);
    LCDletter(' ');
    LCDletter(0);LCDletter(1);

    /* Calculate the starting position of the caller-provided string*/
    nCursorPosition = nDisplayWidth - strlen(szString);
    LCDposition(0x40+nCursorPosition);
    /* and display it */
    LCDputs(szString);
}
